/*
    Copyright 2015 Roberto Sánchez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
/**
 * @page temp temp 
 *
 * @brief Temp converts from one temperature scale to another
 *
 * @section SYNOPSIS
 *
 *  temp FROM [TO] QUANTITY
 *
 * @section description DESCRIPTION
 *	Converts the value QUANTITY in the FROM scale to its value in the TO scale or in any scale, should the latter remain usused
 * @section authors AUTOR
 * Roberto Sanchez (robertosnchez@openmailbox.org)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DIFF  	       32.0
#define DIV 	  	  9
#define MUL 	  	  5
#define TRUE	 	  1
#define FALSE   	  0
#define TOT	 	  3
#define CELSIUS 	 10
#define KELVIN  	 20
#define FAHR		 30
#define EQUAL	 	  0
#define ERR	         -1
#define DIFFK  		273
#define SENTENCE	500

double celsius, fahr, kelvin, value;

void CtoK()
{
	kelvin=value + DIFFK;
}

void KtoC()
{
	celsius=value - DIFFK;
}

void FtoC()
{
	celsius=(MUL * (value - DIFF))/DIV;

}

void CtoF()
{
	fahr=DIV * value/MUL  + DIFF;
}

void KtoF()
{
	fahr=(DIV * (value - DIFFK))/MUL  + DIFF ;
}

void FtoK()
{
	kelvin=(MUL * (value - DIFF))/DIV + DIFFK;
}

int main(int argc, char ** argv)
{

	int from, to ;
	char str[SENTENCE];
	
	if (argc < TOT){
		printf("Expected arguments\n");
		return ERR;
	}
	if(argc == TOT){
		to=TOT;
		sscanf(argv[TOT -1], "%lf", &value);
	}else{
		if(strcmp(argv[TOT -1],"-c") == EQUAL)
			to=CELSIUS;
		else if (strcmp(argv[TOT -1],"-k") == EQUAL)
                        to=KELVIN;
		else if (strcmp(argv[TOT -1],"-f") == EQUAL)
                        to=FAHR;
		else{
			printf("Wrong argument %s\n", argv[TOT-1]);
			return ERR;
		}
		 sscanf(argv[TOT], "%lf", &value);
	}

	if(strcmp(argv[TOT -2],"-c") == EQUAL)
        	from=CELSIUS;
        else if (strcmp(argv[TOT -2],"-k") == EQUAL)
                from=KELVIN;
        else if (strcmp(argv[TOT -2],"-f") == EQUAL)
       		from=FAHR;
        else{
       		printf("Wrong argument %s\n", argv[TOT -2]);
		return ERR;
	}
	switch(from){
		case CELSIUS:
			if(to == TOT || to == FAHR)
				CtoF();
	                if(to == TOT || to == KELVIN)
                                CtoK();
			sprintf(str,"%.1lf Celsius are ", value);
			break;
		case KELVIN:
			if(to == TOT || to == FAHR)
                                KtoF();
                        if(to == TOT || to == CELSIUS)
                                KtoC();
			sprintf(str,"%.1lf Kelvin  are ", value);
                        break;
		case FAHR:
			if(to == TOT || to == CELSIUS)
                                FtoC();
                        if(to == TOT || to == KELVIN)
                                FtoK();
			sprintf(str,"%.1lf Fahrenheit  are ", value);
                        break;
	}
	if ((to == TOT || to ==CELSIUS) && CELSIUS != from)
                printf("%s%.1lf Celsius\n ", str,celsius);
	if ((to == TOT || to ==KELVIN) && KELVIN != from)
                printf("%s%.1lf Kelvin \n", str,kelvin);
	if ((to == TOT || to ==FAHR) && FAHR != from)
		printf("%s%.1lf Fahrenheit\n ", str,fahr);
	

}
